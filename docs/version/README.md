<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Version Service

* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22version%22%2C%20tier%3D%22sv%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service:Version"

## Logging

* [production.log](/var/log/version/)

## Troubleshooting Pointers

* [Teleport Approver Workflow](../Teleport/teleport_approval_workflow.md)
* [Upgrade camoproxy](../camoproxy/upgrade-camoproxy.md)
* [../ci-runners/ci_pending_builds.md](../ci-runners/ci_pending_builds.md)
* [Cloudflare: Managing Traffic](../cloudflare/managing-traffic.md)
* [Service Locations](../cloudflare/services-locations.md)
* [Chef Guidelines](../config_management/chef-guidelines.md)
* [Chef Vault Basics](../config_management/chef-vault.md)
* [Chef Tips and Tools](../config_management/chef-workflow.md)
* [Chefspec](../config_management/chefspec.md)
* [design.gitlab.com Runbook](../design/design-gitlab-com.md)
* [../elastic/elastic-cloud.md](../elastic/elastic-cloud.md)
* [Gitaly error rate is too high](../gitaly/gitaly-error-rate.md)
* [Upgrading the OS of Gitaly VMs](../gitaly/gitaly-os-upgrade.md)
* [Managing GitLab Storage Shards (Gitaly)](../gitaly/storage-sharding.md)
* [Ad hoc observability tools on Kubernetes nodes](../kube/k8s-adhoc-observability.md)
* [GKE Cluster Upgrade Procedure](../kube/k8s-cluster-upgrade.md)
* [../kube/k8s-oncall-setup.md](../kube/k8s-oncall-setup.md)
* [../kube/k8s-operations.md](../kube/k8s-operations.md)
* [Kubernetes](../kube/kubernetes.md)
* [../logging/logging_gcs_archive_bigquery.md](../logging/logging_gcs_archive_bigquery.md)
* [../monitoring/apdex-alerts-guide.md](../monitoring/apdex-alerts-guide.md)
* [../monitoring/filesystem_alerts_inodes.md](../monitoring/filesystem_alerts_inodes.md)
* [Thanos Compact](../monitoring/thanos-compact.md)
* [Session: Application architecture](../onboarding/architecture.md)
* [GPG Keys for Package Signing](../packaging/manage-package-signing-keys.md)
* [Steps to Recreate/Rebuild the CI CLuster using a Snapshot from the Master cluster (instead of pg_basebackup)](../patroni-ci/rebuild_ci_cluster_from_prod.md)
* [Steps to Create a Standby Patroni CLuster using a Snapshot from the Primary cluster (instead of pg_basebackup)](../patroni/build_cluster_from_snapshot.md)
* [Custom PostgreSQL Package Build Process for Ubuntu Xenial 16.04](../patroni/custom_postgres_packages.md)
* [../patroni/database_peak_analysis.md](../patroni/database_peak_analysis.md)
* [Geo Patroni Cluster Management](../patroni/geo-patroni-cluster.md)
* [Patroni Cluster Management](../patroni/patroni-management.md)
* [Postgresql minor upgrade](../patroni/pg_minor_upgrade.md)
* [../patroni/postgres-checkup.md](../patroni/postgres-checkup.md)
* [postgres_exporter](../patroni/postgres_exporter.md)
* [../patroni/postgresql-backups-wale-walg.md](../patroni/postgresql-backups-wale-walg.md)
* [PostgreSQL VACUUM](../patroni/postgresql-vacuum.md)
* [../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md](../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md)
* [Postgres Replicas](../postgres-dr-delayed/postgres-dr-replicas.md)
* [Praefect has read-only repositories](../praefect/praefect-read-only.md)
* [Removing cache entries from Redis](../redis-cache/remove-cache-entries.md)
* [Redis-Sidekiq catchall workloads reduction](../redis/redis-sidekiq-catchall-workloads-reduction.md)
* [../redis/redis.md](../redis/redis.md)
* [../registry/migration-failure-scenarios.md](../registry/migration-failure-scenarios.md)
* [High Number of Overdue Online GC Tasks](../registry/online-gc-high-overdue-tasks.md)
* [../uncategorized/about-gitlab-com.md](../uncategorized/about-gitlab-com.md)
* [Aptly](../uncategorized/aptly.md)
* [Auto DevOps](../uncategorized/auto-devops.md)
* [Summary](../uncategorized/cloudsql-data-export.md)
* [GitLab dev environment](../uncategorized/dev-environment.md)
* [Managing Chef](../uncategorized/manage-chef.md)
* [Google mtail for prometheus metrics](../uncategorized/mtail.md)
* [Omnibus package troubleshooting](../uncategorized/omnibus-package-updates.md)
* [../uncategorized/osquery.md](../uncategorized/osquery.md)
* [Project exports](../uncategorized/project-export.md)
* [Removing kernels from fleet](../uncategorized/remove-kernels.md)
* [Tweeting Guidelines](../uncategorized/tweeting-guidelines.md)
* [Configuring and Using the Yubikey](../uncategorized/yubikey.md)
* [Vault Secrets Management](../vault/vault.md)
* [Gitaly version mismatch](gitaly-version-mismatch.md)
* [version.gitlab.com Runbook](version-gitlab-com.md)
* [Static repository objects caching](../web/static-repository-objects-caching.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
